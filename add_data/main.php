<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../assets/css/styles.css">
<!--        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">-->
        <link rel="stylesheet" href="../assets/fontawesome/fontawesome.6.2.css">
        <script src="../assets/js/jquery_3.6.js"></script>
        <title>Document</title>
        <style>
            body
                {
                    width:99.8%; max-height:auto;
                    position: static;
                    overflow: scroll ;
                    border: 1px solid gray ;
                    margin-top: 1px; margin-left:1px;
                    z-index: 1;
                    background-color: #CCCCCC;
                }
            header
                {
                    margin-top: 5px;margin-left: 5px;
                }

        </style>
    </head>
    <body>
        <header class="fixed-top  container-fluid mx-0 gx-1">
            <nav class="navbar navbar-expand-lg navbar-light" style="background-color:#a09b9a;">
                <div class="container-fluid">
                    <a class="navbar-brand" href="main.php">Ajax Work</a>
                    <div class="collapse navbar-collapse d-flex justify-content-end" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item mx-1">
                                <a href="division_add/division_add.php" class="nav-link  btn btn-info btn-sm text-danger" aria-current="page" id="division">Division Add</a>
                            </li>
                            <li class="nav-item mx-1">
                                <a href="district/district_add.php" class="nav-link btn btn-info btn-sm text-danger" aria-current="page" id="districts" >Districts Add</a>
                            </li>

                            <li class="nav-item mx-1">
                                <a href="#" class="nav-link btn btn-info btn-sm text-danger" aria-current="page" id="upazila" id="upazila">Upazila Add</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <hr class="mx-1 gx-0" style="color: inherit;background-color: currentColor; border: 0;opacity: 0.25">
        </header>
     <div id="box" style="margin-top:75px; height: 650px" class="overflow-scroll"></div>
          <script>
           $(document).ready(function ($) {
                $("#division").click(function (event) {
                    $("#division").addClass("active");
                    link = $(this).attr("href");
                    $.ajax({
                            url: link,
                        })
                        .done(function (html) {
                            $("#box").empty().append(html);
                        })
                    return false;
                });
            });
            // Districts Add
            $(document).ready(function ($) {
                $("#districts").click(function (event) {
                    link = $(this).attr("href");
                    $.ajax({
                            url: link,
                        })
                        .done(function (html) {
                            $("#box").empty().append(html);
                        })
                    return false;
                });
            });

      </script>
    </body>
</html>