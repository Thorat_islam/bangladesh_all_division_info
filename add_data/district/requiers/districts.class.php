<?php
require("../../../db_config.php");
class Districts
{
    //id, division_id, name, established, iso, hdi, status_active, is_delete, create_at, update_at, code
    public $id;
    public $division_id;
    public $district_name;
    public $bang_name;
    public $established;
    public $post_code;
    public $iso;
    public $hdi;
    public $status_active;
    public $is_delete;
    public $create_at;
    public $update_at;
    public $code;

    function __construct($id, $division_id, $district_name,$bang_name, $established,$post_code, $iso, $hdi, $status_active, $is_delete, $create_at, $update_at, $code)
    {
        $this->id=$id;
        $this->division_id=$division_id;
        $this->district_name=$district_name;
        $this->bang_name=$bang_name;
        $this->established=$established;
        $this->post_code=$post_code;
        $this->iso=$iso;
        $this->hdi=$hdi;
        $this->status_active=$status_active;
        $this->is_delete=$is_delete;
        $this->create_at=$create_at;
        $this->update_at=$update_at;
        $this->code=$code;
    }
    function insert(){
        global $db;
        //id, division_id, name, bang_name, established, iso, hdi, status_active, is_delete, create_at, update_at, code
        //echo "INSERT into tbl_districts (division_id, name,bang_name, established, iso, hdi, status_active, is_delete, create_at)values('$this->division_id','$this->district_name','$this->bang_name','$this->established','$this->iso','$this->hdi','$this->status_active','$this->is_delete','$this->create_at')";die;
        $db->query("INSERT into tbl_districts (division_id, name,bang_name, established, post_code,iso, hdi, status_active, is_delete, create_at)
            values('$this->division_id','$this->district_name','$this->bang_name',$this->established,'$this->post_code','$this->iso','$this->hdi','$this->status_active','$this->is_delete','$this->create_at')");
        return $db->insert_id;
    }


}