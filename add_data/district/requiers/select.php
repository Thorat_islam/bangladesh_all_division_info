<?php require("../../../db_config.php");
$sql="SELECT a.id as district_id,a.division_id,b.division_name, a.name, a.bang_name, a.established, a.iso, a.hdi, a.status_active, a.is_delete, a.create_at, a.update_at, a.code,a.post_code from tbl_districts a,tbl_divisions b Where a.division_id=b.id and a.status_active=1 and a.is_delete=0 order by division_id";
//echo $sql;
$result=$db->query($sql);
$districts_info_arr=[];
foreach ($result as $row)
{
    $districts_info_arr[$row['division_id']][$row['district_id']]['district_id']=$row["district_id"];
    $districts_info_arr[$row['division_id']][$row['district_id']]['division_id']=$row["division_id"];
    $districts_info_arr[$row['division_id']][$row['district_id']]['division_name']=$row["division_name"];
    $districts_info_arr[$row['division_id']][$row['district_id']]['district_name']=$row["name"];
    $districts_info_arr[$row['division_id']][$row['district_id']]['district_bang_name']=$row["bang_name"];
    $districts_info_arr[$row['division_id']][$row['district_id']]['post_code']=$row["post_code"];
    $districts_info_arr[$row['division_id']][$row['district_id']]['established']=$row["established"];
    $districts_info_arr[$row['division_id']][$row['district_id']]['iso']=$row["iso"];
    $districts_info_arr[$row['division_id']][$row['district_id']]['hdi']=$row["hdi"];
}

?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
<table width="920" class="table table-bordered border-primary" style="margin-top:-15.2px">
    <tbody>
    <?php
    $i=1;
    foreach ($districts_info_arr as $division_id)
    {
        foreach ($division_id as $row)
        {
            $counr=$division_id;
        ?>
        <tr style="text-align: center;">
            <td width="40" valign="middle"><?php echo $i;?></td>
            <td width="120" valign="middle" rowspan="<?php echo $counr;?>"><?php echo $row["division_name"];?></td>
            <td width="120" valign="middle"><?php echo $row["district_name"];?></td>
            <td width="120" valign="middle"><?php echo $row["district_bang_name"];?></td>
            <td width="120" valign="middle"><?php echo $row["post_code"];?></td>
            <td width="100" valign="middle"><?php echo $row["established"];?></td>

            <td width="100" valign="middle"><?php echo $row["iso"];?></td>
            <td width="80" valign="middle"><?php echo $row["hdi"];?></td>
            <td width="120" valign="middle">
                <button type="button" id="del" class="btn btn-danger btn-sm"
                        data-id="<?php echo $row["district_id"];?>">
                    <i class="fa fa-solid fa-trash-can" style="width: 10px"></i>
                </button>
                <button type="button" id="edit_dist" class="btn btn-primary btn-sm"
                        data-id="<?php echo $row["district_id"];?>"
                        data-division_name="<?php echo $row["division_name"];?>"
                        data-district_name="<?php echo $row["district_name"];?>"
                        data-division_bng_name="<?php echo $row["district_bang_name"];?>"
                        data-established="<?php echo $row["established"];?>"
                        data-post_code="<?php echo $row["post_code"];?>"
                        data-iso="<?php echo $row["iso"];?>"
                        data-hdi="<?php echo $row["hdi"];?>"
                ><i class="fa-solid fa-pen-to-square" style="width: 10px"></i>
                </button>
            </td>
        </tr>
        <?php
        $i++;
    }
    }
    ?>
    </tbody>
</table>