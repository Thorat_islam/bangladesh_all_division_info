<?php require("../../db_config.php");?>
<div class="container-fluid mx-0 gx-0" style="margin-top:5px; height: 650px">
    <div class="row g-1">
        <div class="col-md-4 position-fixed" >
            <table class="table table-bordered border-primary" >
                <tr class="text-center mw-100">
                    <th colspan="3">
                        <div>
                            <h3>District Add</h3>
                        </div>
                        <div id="masg" class=""></div>
                    </th>
                </tr>
                <?php
                    $sql_division="SELECT id,division_name from tbl_divisions";
                    $divisions=$db->query($sql_division);
                ?>
                <form autocomplete="off" id="form">
                    <input type="hidden" id="txtId" />
                    <tr >
                        <th>
                            <label class="form-label" for="txtdistrict_name">Division Name</label>
                        </th>
                        <td class="cotetion">:</td>
                        <td>
                            <select name="cbo_division_name" id="cbo_division_name" class="w-100">
                                <option selected disabled>--Select Division--</option>
                                <?php
                                    foreach ($divisions as $division)
                                    {
                                ?>
                                        <option value="<?php echo $division['id']?>">
                                            <?php echo $division['division_name']?>
                                        </option>
                                <?php
                                    }
                                 ?>
                                </select>
                        </td>
                        </tr>
                        <tr >
                            <th >
                                <label class="form-label" for="txtdistrict_name">District Name</label>
                            </th>
                            <td class="cotetion">:</td>
                            <td>
                                <input class="w-100" type="text" id="txtdistrict_name" disabled required/>
                            </td>
                        </tr>
                        <tr>
                            <th  class="label">
                                <label for="txtdistrict_bng_name">District Bangla Name</label>
                            </th>
                            <td class="cotetion">:</td>
                            <td>
                                <input class="w-100" type="text" id="txtdistrict_bng_name" disabled required/>
                                <span id="divbngrequired" class=""></span>
                            </td>
                        </tr>
                        <tr>
                            <th  class="label">
                                <label for="txtestablished">Established</label>
                            </th>
                            <td class="cotetion">:</td>
                            <td>
                                <input class="w-100" type="number" id="txtestablished" disabled required />
                                <span id="establishedrequired" class=""></span>
                            </td>
                        </tr>
                        <tr>
                            <th  class="label">
                                <label for="txtpost_code">Postal code</label>
                            </th>
                            <td class="cotetion">:</td>
                            <td>
                                <input class="w-100" type="number" id="txtpost_code" disabled required />
                                <span id="post_code_required" class=""></span>
                            </td>
                        </tr>
                        <tr>
                            <th  class="label">
                                <label for="txtiso" title="International Organization for Standardization  3166">ISO</label>
                            </th>
                            <td class="cotetion">:</td>
                            <td>
                                <input class="w-100" type="text" id="txtiso" disabled required />
                                <span id="isorequired" class=""></span>
                            </td>
                        </tr>
                        <tr>
                            <th  class="label">
                                <label for="txthdi" title="Human Development Index (HDI)">HDI</label>
                            </th>
                            <td class="cotetion">:</td>
                            <td>
                                <input class="w-100" type="text" id="txthdi" disabled required/>
                                <span id="hdirequired" class=""></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right;">
                                <button type="button"  id="btnSubmit" class="btn btn-primary btn-sm " disabled>Save</button>
                                <button type="button" id="btnClear"  class="btn btn-secondary btn-sm">Clear</button>
                            </td>
                        </tr>
                    </form>
            </table>
        </div>
        <div class="col-md-8" style="margin-left: 510px">
            <table width="920" class="table table-bordered border-primary " >
                <thead>
                <tr class="text-center mw-100">
                    <th colspan="9">
                        <div>
                            <h3>District List</h3>
                        </div>
                        <div id="masg" class=""></div>
                    </th>
                </tr>
                <tr class="text-center">
                    <th width="40" valign="middle">#</th>
                    <th width="120" valign="middle">Division</th>
                    <th width="120" valign="middle">Name</th>
                    <th width="120" valign="middle">বাংলা নাম</th>
                    <th width="120" valign="middle">Postal code</th>

                    <th width="100" valign="middle">Established</th>
                    <th width="100" valign="middle" title="International Organization for Standardization (ISO)">ISO</th>
                    <th width="80" valign="middle" title="Human Development Index (HDI)">HDI</th>
                    <th width="120" valign="middle">Action</th>
                </tr>
                </thead>
            </table>
            <div id="datatable" width="940"></div>
            </fieldset>
        </div>
    </div>
</div>
<script>
    $(function ()
    {
        data_list_table();

        $("body").on("click","#edit_dist",function(){
            let id = $(this).data("id");
            let division_name = $(this).data("division_name");
            let division_bng_name =$(this).data("division_bng_name");
            let established = $(this).data("established");
            let iso = $(this).data("iso");
            let hdi = $(this).data("hdi");
            $("#txtId").val(id);
            $("#cbo_division_name").append("<option>"+division_name+"</option>");
            $("#txtdivision_bng_name").val(division_bng_name).removeAttr("disabled");
            $("#txtestablished").val(established).removeAttr("disabled");
            $("#txtiso").val(iso).removeAttr("disabled");
            $("#txthdi").val(hdi).removeAttr("disabled");
            $("#btnSubmit").text("Update");
        });
        //==================Input Field Enable ==========================
            $('#cbo_division_name').on('change',function ()
            {
                $('#txtdistrict_name').removeAttr("disabled");
                $('#txtdistrict_name').on('keypress',function ()
                {
                    $('#txtdistrict_bng_name').removeAttr("disabled");

                    $('#txtdistrict_bng_name').on('keypress',function ()
                    {
                        $('#txtestablished').removeAttr("disabled");
                        $('#txtestablished').on('keypress',function ()
                        {
                            $('#txtpost_code').removeAttr("disabled");
                            $('#txtpost_code').on('keypress',function ()
                            {
                                $('#txtiso').removeAttr("disabled");
                                $('#txtiso').on('keypress',function ()
                                {
                                    $('#txthdi').removeAttr("disabled");
                                    $('#txthdi').on('keypress',function ()
                                    {
                                        $('#btnSubmit').removeAttr("disabled");
                                    });
                                });
                            });

                        });
                    });
                });
            });
        //===============================================================

        $('#btnSubmit').on('click',function ()
        {
            let division_id=$('#cbo_division_name').val();
            let district_name=$('#txtdistrict_name').val();
            let district_bng_name=$('#txtdistrict_bng_name').val();
            let established=$('#txtestablished').val();
            let txtpost_code=$('#txtpost_code').val();
            let iso=$('#txtiso').val();
            let hdi=$('#txthdi').val();
            //alert($('#txtpost_code').val());

             if (district_name == '')
            {
                $("#masg").addClass("text-danger").html("District Input field is Required");
            }
            else if (district_bng_name == '')
            {
                $("#masg").addClass("text-danger").html("District Bangla Input field is Required");
            }
            else if (established == '')
            {
                $("#masg").addClass("text-danger").html("Established Input field is Required");
            }
            else if (txtpost_code == '')
            {

                $("#masg").addClass("text-danger").html("Postal Code Input field is Required");
            }
            else if (iso == '') {
                $("#masg").addClass("text-danger").html("ISO Input field is Required");
            } else if (hdi=='') {
                $("#masg").addClass("text-danger").html("HDI Input field is Required");
            }
            else
            {
                if ($("#btnSubmit").text() == "Save")
                {
                    $.ajax
                    ({
                        url: 'district/requiers/insert.php',
                        type: 'post',
                        data:
                            {
                                'cbo_division_name': division_id,
                                'txtdistrict_name': district_name,
                                'txtdistrict_bng_name': district_bng_name,
                                'txtestablished': established,
                                'txtpost_code': txtpost_code,
                                'txtiso': iso,
                                'txthdi': hdi,
                                'btnSubmit': 'Submit',
                            },
                        success: function (res) {
                            //alert(res)
                            if (res > 0) {
                                $("#masg").addClass("alert alert-success p-1").html("Your Input Data is Save");
                                clearForm();
                                data_list_table();
                            } else if (res == "Duplicate")
                            {
                                $("#masg").addClass("alert alert-danger text-danger p-1").html("Duplicate Data");
                            }
                        }
                    });
                }
                if ($("#btnSubmit").text() == "Update"){

                }
             }
        });
        $("#btnClear").on("click", function ()
        {
            clearForm();
            $("#btnSubmit").text("Save");
        });

        // ============Data List Table=========
        function data_list_table()
        {
            $.ajax({
                url: 'district/requiers/select.php',
                type: 'post',
                data: {},
                success: function (result) {
                    $("#datatable").html(result);
                }
            });
        }

        function clearForm()
        {
            $("#cbo_division_name").val("");
            $("#cbo_division_name").append('<option disabled selected>--Select Division--</option>');
            $("#txtdistrict_name").val("");
            $("#txtdistrict_bng_name").val("");
            $("#txtestablished").val("");
            $("#txtpost_code").val("");
            $("#txtiso").val("");
            $("#txthdi").val("");
            $("#divrequired").text("");
            $("#divbngrequired").text("");
        }
    });

</script>

