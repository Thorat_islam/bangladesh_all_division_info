<?php require("../../db_config.php");?>
<div class="container-fluid mx-0 gx-0" style="margin-top:5px; height: 650px">
    <div class="row g-1">
        <div class="col-md-4" >
            <table class="table table-bordered border-primary">
                <tr class="text-center mw-100">
                    <th colspan="3">
                        <div>
                           <h3>Division Add</h3>
                        </div>
                        <div id="masg" class=""></div>
                    </th>
                </tr>
                <?php
                    $sql="SELECT count(*) as total_division from tbl_divisions";
                    //echo $sql;
                    $result=$db->query($sql);
                    list($total_division)=$result->fetch_row();
                    if ($total_division<8)
                    {
                        ?>
                            <form autocomplete="off" id="form">
                        <input type="hidden" id="txtId" />
                        <tr >
                            <th >
                                <label class="form-label" for="txtdivision_name">Division Name</label>
                            </th>
                            <td class="cotetion">:</td>
                            <td>
                                <input class="w-100" type="text" id="txtdivision_name" required/>


                                <span id="divrequired" class="" ></span>
                            </td>
                        </tr>
                        <tr>
                            <th  class="label">
                                <label for="txtdivision_bng_name">Division Bangla Name</label>

                            </th>
                            <td class="cotetion">:</td>
                            <td>
                                <input class="w-100" type="text" id="txtdivision_bng_name" required/>
                                <span id="divbngrequired" class=""></span>
                            </td>
                        </tr>
                        <tr>
                            <th  class="label">
                                <label for="txtestablished">Established</label>
                            </th>
                            <td class="cotetion">:</td>
                            <td>
                                <input class="w-100" type="text" id="txtestablished" required />
                                <span id="establishedrequired" class=""></span>
                            </td>
                        </tr>
                        <tr>
                            <th  class="label">
                                <label for="txtiso" title="International Organization for Standardization  3166">ISO</label>
                            </th>
                            <td class="cotetion">:</td>
                            <td>
                                <input class="w-100" type="text" id="txtiso" required />
                                <span id="isorequired" class=""></span>
                            </td>
                        </tr>
                        <tr>
                            <th  class="label">
                                <label for="txthdi" title="Human Development Index (HDI)">HDI</label>
                            </th>
                            <td class="cotetion">:</td>
                            <td>
                                <input class="w-100" type="text" id="txthdi" required/>
                                <span id="hdirequired" class=""></span>
                            </td>
                        </tr>
                        <tr>
<!--                            <th  class="label">&nbsp;</th>-->
<!--                            <td class="cotetion">:</td>-->
                            <td colspan="3" style="text-align: right;">
                                <button type="button"  id="btnSubmit" class="btn btn-primary btn-sm" >Save</button>
                                <button type="button" id="btnClear"  class="btn btn-secondary btn-sm">Clear</button>

                            </td>
                        </tr>
                    </form>
                        <?php
                    }else
                    {
                        ?>
                            <form autocomplete="off" id="">
                            <input type="hidden" id="txtId" />
                            <tr>
                                <th>
                                    <label class="form-label" for="txtdivision_name">Division Name</label>
                                </th>
                                <td class="cotetion">:</td>
                                <td>
                                    <input class="w-100" type="text" id="txtdivision_name" disabled required/>

                                    <span id="divrequired" class="" ></span>
                                </td>
                            </tr>
                            <tr>
                                <th  class="label">
                                    <label for="txtdivision_bng_name">Division Bangla Name</label>

                                </th>
                                <td class="cotetion">:</td>
                                <td>
                                    <input class="w-100" type="text" id="txtdivision_bng_name" disabled required/>
                                    <span id="divbngrequired" class=""></span>
                                </td>
                            </tr>
                            <tr>
                                <th  class="label">
                                    <label for="txtestablished">Established</label>
                                </th>
                                <td class="cotetion">:</td>
                                <td>
                                    <input class="w-100" type="text" id="txtestablished" disabled required />
                                    <span id="establishedrequired" class=""></span>
                                </td>
                            </tr>
                            <tr>
                                <th  class="label">
                                    <label for="txtiso" title="International Organization for Standardization  3166">ISO</label>
                                </th>
                                <td class="cotetion">:</td>
                                <td>
                                    <input class="w-100" type="text" id="txtiso" disabled required />
                                    <span id="isorequired" class=""></span>
                                </td>
                            </tr>
                            <tr>
                                <th  class="label">
                                    <label for="txthdi" title="Human Development Index (HDI)">HDI</label>
                                </th>
                                <td class="cotetion">:</td>
                                <td>
                                    <input class="w-100" type="text" id="txthdi" disabled required/>
                                    <span id="hdirequired" class=""></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: right;">
                                    <button type="button"  id="btnSubmit" class="btn btn-primary btn-sm" >Save</button>
                                    <button type="button" id="btnClear"  class="btn btn-secondary btn-sm">Clear</button>

                                </td>
                            </tr>
                        </form>
                        <?php
                    }
                        ?>
            </table>
        </div>
        <div class="col-md-8 ">
         <table width="580" class="table table-bordered border-primary" id="dataList">
                 <thead>
                     <tr class="text-center mw-100">
                    <th colspan="8">
                        <div>
                           <h3>Division List</h3>
                        </div>
                        <div id="masg" class=""></div>
                    </th>
                </tr>
                    <tr class="text-center">
                        <th width="40" valign="middle">#</th>
                        <th width="120" valign="middle">Name</th>
                        <th width="120" valign="middle">বাংলা নাম</th>
                        <th width="120" valign="middle">Capital</th>
                        <th width="100" valign="middle">Established</th>
                        <th width="100" valign="middle" title="International Organization for Standardization (ISO)">ISO</th>
                        <th width="80" valign="middle" title="Human Development Index (HDI)">HDI</th>
                        <th width="120" valign="middle">Action</th>
                    </tr>
                </thead>
            </table>
                <div id="datatable" width="600"></div>
            </fieldset>
        </div>
    </div>
</div>
<script >
$(function () 
{
        data_list_table();
    $("body").on("click","#edit",function(){
        let id = $(this).data("id");
        let division_name = $(this).data("division_name");
        let division_bng_name =$(this).data("division_bng_name");
        let established = $(this).data("established");
        let iso = $(this).data("iso");
        let hdi = $(this).data("hdi");
        $("#txtId").val(id);
        $("#txtdivision_name").val(division_name).removeAttr("disabled");
        $("#txtdivision_bng_name").val(division_bng_name).removeAttr("disabled");
        $("#txtestablished").val(established).removeAttr("disabled");
        $("#txtiso").val(iso).removeAttr("disabled");
        $("#txthdi").val(hdi).removeAttr("disabled");
        $("#btnSubmit").text("Update");
    });
    $("body").on("click","#del",function()
    {
        // $("#exampleModal").click("#del");
        let delte_data=$(this).data('id');
        //alert(delte_data);
        if (delte_data == '1' || delte_data == '2' || delte_data == '3' || delte_data == '4' || delte_data == '5' || delte_data == '6' || delte_data == '7' || delte_data == '8')
        {
            alert(delte_data);
        }
    });

    $("#btnSubmit").on("click", function ()
        {
            let id = $("#txtId").val();
            let division_name = $("#txtdivision_name").val();
            let division_bng_name = $("#txtdivision_bng_name").val();
            let established = $("#txtestablished").val();
            let iso = $("#txtiso").val();
            let hdi = $("#txthdi").val();

            if (division_name == '')
            {
                $("#masg").addClass("text-danger").html("Division Input field is Required");
            }
            else if (division_bng_name == '')
            {
                
                $("#masg").addClass("text-danger").html("Division Bangla Input field is Required");
            } else if (established == '')
            {
                $("#masg").addClass("text-danger").html("Established Input field is Required");
            } else if (iso == '') {
                $("#masg").addClass("text-danger").html("ISO Input field is Required");
            } else if (hdi=='') {
                $("#masg").addClass("text-danger").html("HDI Input field is Required");
            }
            else 
            {
                //alert(id+division_name+division_bng_name+established+iso+hdi);
                $("#txtdivision_name").attr('required', '');
                $("#txtdivision_bng_name").attr('required', '');
                $("#txtestablished").attr('required', '');
                $("#txtiso").attr('required', '');
                $("#txthdi").attr('required', '');

                if ($("#btnSubmit").text() == "Save") 
                {

                    $.ajax
                    ({
                        url: 'division_add/requires/insert.php',
                        type: 'post',
                        data: 
                        {
                            'txtdivision_name': division_name,
                            'txtdivision_bng_name': division_bng_name,
                            'txtestablished': established,
                            'txtiso': iso,
                            'txthdi': hdi,
                            'btnSubmit': 'Submit',
                        },
                        success: function (res) {
                            if (res > 0) {
                                $("#masg").addClass("alert alert-success p-1").html("Your Input Data is Save");
                                clearForm();
                                data_list_table();
                            } else if (res == "Duplicate") 
                            {
                                $("#masg").addClass("alert alert-danger text-danger p-1").html("Duplicate Data");
                            }                        
                        }
                    });
                }
                if ($("#btnSubmit").text() == "Update")
                {
                    $.ajax
                    ({
                        url: 'division_add/requires/update.php',
                        type: 'post',
                        data:
                            {
                                'txtId':id,
                                'txtdivision_name': division_name,
                                'txtdivision_bng_name': division_bng_name,
                                'txtestablished': established,
                                'txtiso': iso,
                                'txthdi': hdi,
                                'btnSubmit': 'Submit',
                            },
                        success: function (res) {
                            if (res == 1) {
                                $("#masg").addClass("alert alert-success p-1").html("Your Input Data is Update");
                                clearForm();
                                disable();
                                data_list_table();
                            } else
                            {
                                $("#masg").addClass("alert alert-danger text-danger p-1").html("Update Error");
                            }
                        }
                    });
                }
            }
        });
        // ===========Edit data read

        // ============Data List Table=========
        function data_list_table()
        {
        $.ajax({
            url: 'division_add/requires/select.php',
            type: 'post',
            data: {},
            success: function (result) {
                $("#datatable").html(result);
            }
        });
        }
        //<=======Form Clear =============//
        $("#btnClear").on("click", function ()
        {
            clearForm();
            $("#btnSubmit").text("Save");
        });

        function clearForm() {
        $("#txtId").val(1);
        $("#txtdivision_name").val("");
        $("#txtdivision_bng_name").val("");
        $("#txtestablished").val("");
        $("#txtiso").val("");
        $("#txthdi").val("");
        $("#divrequired").text("");
        $("#divbngrequired").text("");
        }
        function disable(){
            $('#txtdivision_name').attr('disabled','');
            $('#txtdivision_bng_name').attr('disabled','');
            $('#txtestablished').attr('disabled','');
            $('#txtiso').attr('disabled','');
            $('#txthdi').attr('disabled','');
        }
    //======================================
});

</script>
