<?php require("../../../db_config.php");
class Division{
	public $id;
	public $division_name;
	public $division_bng_name;

	public $established;
	public $iso;
	public $hdi;
	public $status_active;
	public $is_delete;
	public $created_at;

	function __construct($id,$division_name,$division_bng_name,$established,$iso,$hdi,$status_active,$is_delete,$created_at)
	{
		$this->id=$id;
		$this->division_name=$division_name;
		$this->division_bng_name=$division_bng_name;

		$this->established=$established;
		$this->iso=$iso;
		$this->hdi=$hdi;
		$this->status_active=$status_active;
		$this->is_delete=$is_delete;
		$this->created_at=$created_at;
	}
	function insert(){
		global $db;
		$db->query("INSERT into tbl_divisions (division_name, capital_name, established, iso, hdi, create_at, division_bng_name)values('$this->division_name','$this->division_name','$this->established','$this->iso','$this->hdi',now(),'$this->division_bng_name')");
		return $db->insert_id;
	}
    function update ()
    {
        global $db;
        $db->query("Update tbl_divisions set division_name='$this->division_name',capital_name='$this->division_name',division_bng_name='$this->division_bng_name',established='$this->established',iso='$this->iso',hdi='$this->hdi' where id='$this->id'");
    }
    static function delete($id){
        global $db;
        $db->query("delete from tbl_divisions where id='$id'");
    }
	
}
?>