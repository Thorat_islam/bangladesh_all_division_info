<?php require("../../../db_config.php");
$sql="SELECT id, division_name, capital_name, established, iso, hdi, status_active, is_delete, create_at, division_bng_name from tbl_divisions";
//echo $sql;
$result=$db->query($sql);
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
<table width="580" class="table table-bordered border-primary" style="margin-top:-15.2px">
   <tbody>
       <?php
       $i=1;
       foreach ($result as $row) 
       {
           ?>
           <tr>
               <td width="40" valign="middle"><?php echo $i;?></td>
               <td width="120" valign="middle"><?php echo $row["division_name"];?></td>
               <td width="120" valign="middle"><?php echo $row["division_bng_name"];?></td>
               <td width="120" valign="middle"><?php echo $row["capital_name"];?></td>
               <td width="100" valign="middle"><?php echo $row["established"];?></td>
               <td width="100" valign="middle"><?php echo $row["iso"];?></td>
               <td width="80" valign="middle"><?php echo $row["hdi"];?></td>
               <td width="120" valign="middle">
                 <button type="button" id="del" class="btn btn-danger btn-sm"
                         data-id="<?php echo $row["id"];?>">
                     <i class="fa fa-solid fa-trash-can" style="width: 10px"></i>
                 </button>
                 <button type="button" id="edit" class="btn btn-primary btn-sm"
                   data-id="<?php echo $row["id"];?>" 
                   data-division_name="<?php echo $row["division_name"];?>" 
                   data-division_bng_name="<?php echo $row["division_bng_name"];?>" 
                   data-capital_name="<?php echo $row["capital_name"];?>" 
                   data-established="<?php echo $row["established"];?>" 
                   data-iso="<?php echo $row["iso"];?>" 
                   data-hdi="<?php echo $row["hdi"];?>" 
                   ><i class="fa-solid fa-pen-to-square" style="width: 10px"></i>
                 </button>
               </td>
           </tr>
           <?php
           $i++;
       }
       ?>
   </tbody>
</table>