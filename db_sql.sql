CREATE TABLE `db_division_ajax`.`tbl_divisions` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `division_name` VARCHAR(45) NOT NULL DEFAULT '',
  `capital_name` VARCHAR(45) NOT NULL DEFAULT '',
  `established` VARCHAR(45) NOT NULL DEFAULT '',
  `iso` VARCHAR(45) NOT NULL DEFAULT '',
  `hdi` VARCHAR(45) NOT NULL DEFAULT '',
  `status_active` VARCHAR(45) NOT NULL DEFAULT '0',
  `is_delete` VARCHAR(45) NOT NULL DEFAULT '0',
  `create_at` TIMESTAMP,
  PRIMARY KEY(`id`)
)
ENGINE = InnoDB;
ALTER TABLE `db_division_ajax`.`tbl_divisions` MODIFY COLUMN `status_active` INTEGER UNSIGNED NOT NULL DEFAULT 0,
 MODIFY COLUMN `is_delete` INTEGER UNSIGNED NOT NULL DEFAULT 0;
 ALTER TABLE `db_division_ajax`.`tbl_divisions` ADD COLUMN `division_bng_name` VARCHAR(45) NOT NULL DEFAULT '' AFTER `create_at`;


REATE TABLE `db_division_ajax`.`tbl_districts` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `district_name` VARCHAR(45) NOT NULL DEFAULT '',
  `division_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `established` VARCHAR(45) NOT NULL DEFAULT '',
  `area` VARCHAR(45) NOT NULL DEFAULT '',
  `population` VARCHAR(45) NOT NULL DEFAULT '',
  `density` VARCHAR(45) NOT NULL DEFAULT '',
  `status_active` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `is_delete` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  `create_at` TIMESTAMP,
  PRIMARY KEY(`id`)
)
ENGINE = InnoDB;
ALTER TABLE `db_division_ajax`.`tbl_districts` ADD COLUMN `districts_bng_name` VARCHAR(45) NOT NULL DEFAULT '' AFTER `create_at`;

