<?php require("db_config.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Landing Page - Start Bootstrap Theme</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    <!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" type="text/css" />
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="assets/css/styles.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/js/jquery-ui.css">
    
</head>
<body>
    <!-- Navigation-->
    <nav class="navbar navbar-light bg-light static-top">
        <div class="container">
            <a class="navbar-brand" href="#!">
                <img src="assets/img/testimonials-1.jpg" alt="" srcset="" width="50" style="border-radius: 50%;vertical-align:middle;">&nbsp;Ajax Work
            </a>
            <a class="btn btn-primary" target="_blank" href="add_data/main.php">Sign Up</a>
        </div>
    </nav>
    <!-- Masthead-->
    <header class="masthead" style="padding-top: 2rem;padding-bottom: 2rem;">
        <div class="container position-relative">
            <div class="row justify-content-center">
                <div class="col-xl-12">
                    <div class="text-center text-white">
                        <!-- Page heading-->
                        <h1 class="mb-5">Bangladesh All Divisional Information!</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <form class="form-subscribe" id="contactForm">
                    <div class="row">
                        <div class="col-xl-4">
                            <select class="form-control form-control-lg" name="" id="cmbDivision">
                                <option selected disabled>Select Your Division</option>
                                <?php 
                                 
                                $result=$db->query("SELECT id, division_name,division_bng_name from tbl_divisions");
                                foreach($result as $row){
                                    ?>
                                    <option value="<?php echo $row["id"];?>"><?php echo $row["division_name"]."&nbsp;(".$row["division_bng_name"].")";?></option>
                                     
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-xl-4">
                            <select class="form-control form-control-lg" name="" id="cboDistricts">
                                <option selected disabled>Select Your Districts</option>
                                
                            </select>
                        </div>
                        <div class="col-xl-4">
                            <select class="form-control form-control-lg" name="" id="cboUpazila">
                                <option selected disabled>Select Your Upazila</option>
                            </select>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </header>
    <!-- Icons Grid-->
    <section class="features-icons bg-light text-center" style="padding-top: 2rem;">
        <div class="container">
            <div class="row">
               <dv id="tb_division"></dv>
            </div>
        </div>
        
    </section>

<!--
all bangladesh upazilla info

https://www.bangladesh.gov.bd/site/view/upazila-list/%E0%A6%89%E0%A6%AA%E0%A6%9C%E0%A7%87%E0%A6%B2%E0%A6%BE%E0%A6%B8%E0%A6%AE%E0%A7%82%E0%A6%B9 -->

    <!-- Testimonials-->
    <section class="testimonials text-center bg-light ">
        <div class="container ">
            <h2 class="mb-5 ">গণপ্রজাতন্ত্রী বাংলাদেশ ...</h2>
            <div class="row ">
                <div class="col-lg-4 ">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0 ">
                        <img class="img-fluid rounded-circle mb-3 " src="assets/img/division.jpg" alt="..." />
                        <h5>Division</h5>
                        <p class="font-weight-light mb-0" style="text-align: justify">বাংলাদেশ আটটি প্রধান প্রশাসনিক অঞ্চলে বিভক্ত যাদের বাংলায় বিভাগ হিসেবে অভিহিত করা হয়। প্রত্যেকটি বিভাগের নাম ওই অঞ্চলের প্রধান শহরের নামে নামকরণ করা হয়েছে।</p>
                    </div>
                </div>
                <div class="col-lg-4 ">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0 ">
                        <img class="img-fluid rounded-circle mb-3 " src="assets/img/districts.png" alt="... " />
                        <h5>Districts</h5>
                        <p class="font-weight-light mb-0" style="text-align: justify">জেলা বাংলাদেশের প্রশাসনিক ব্যবস্থায় একটি গুরুত্বপূর্ণ স্তর। কয়েকটি উপজেলা নিয়ে একটি জেলা গঠিত হয়। প্রশাসনিকভাবে একটি জেলা একটি বিভাগের অধিক্ষেত্রভুক্ত। ১৯৭১-এ বাংলাদেশ প্রতিষ্ঠাকালে জেলার সংখ্যা ছিল ১৯ টি । রাষ্ট্রপতি এরশাদ মহুকুমাগুলোকে জেলায় উন্নীতকরণের প্রক্রিয়া চালু করেন। ১৯৪৭ খ্রিষ্টাব্দে পাকিস্তান প্রতিষ্ঠালগ্নে পূর্ব পাকিস্তান তথা বর্তমান বাংলাদেশের জেলার সংখ্যা ছিল ১৭ টি। ১৯৬৯-এ ময়মনসিংহ জেলার টাঙ্গাইল মহকুমা ও বরিশাল জেলার পটুয়াখালী মহুকুমাকে জেলায় উন্নীত করা হয়। স্বাধীন বাংলাদেশে সর্বপ্রথম ১৯৭৮ সালে ময়মনসিংহের জামালপুর মহুকুমাকে একটি জেলায় উন্নীত করা হয়।</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                        <img class="img-fluid rounded-circle mb-3" src="assets/img/upazila.jpg" alt="..." />
                        <h5>Upazilla</h5>
                        <p class="font-weight-light mb-0" style="text-align: justify">উপজেলা হচ্ছে বাংলাদেশের প্রশাসনিক ব্যবস্থায় একটি গুরুত্বপূর্ণ একক। কয়েকটি গ্রাম বা ইউনিয়ন মিলে একটি উপজেলা গঠিত হয় এবং কয়েকটি উপজেলা নিয়ে একটি জেলা গঠিত হয়। বর্তমানে বাংলাদেশের ৮টি বিভাগের অন্তর্গত ৬৪টি জেলায় মোট ৪৯২ টি উপজেলা রয়েছে।[১] সর্বশেষ গঠিত উপজেলাগুলি হল মাদারীপুরের ডাসার উপজেলা, কক্সবাজারের ঈদগাঁও উপজেলা ও সুনামগঞ্জের মধ্যনগর উপজেলা। উপজেলা শব্দটি সংস্কৃত ও আরবি ভাষার সংমিশ্রণে সৃষ্টি হয়েছে। উপ হলো সংস্কৃত উপসর্গ আর জেলা শব্দটি আরবি শব্দ "দিলা" থেকে উৎপন্ন।</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer-->
    <footer class="footer bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 h-100 text-center text-lg-start my-auto">
                    <ul class="list-inline mb-2">
                        <li class="list-inline-item"><a href="#!">About</a></li>
                        <li class="list-inline-item">⋅</li>
                        <li class="list-inline-item"><a href="#!">Contact</a></li>
                        <li class="list-inline-item">⋅</li>
                        <li class="list-inline-item"><a href="#!">Terms of Use</a></li>
                        <li class="list-inline-item">⋅</li>
                        <li class="list-inline-item"><a href="#!">Privacy Policy</a></li>
                    </ul>
                    <p class="text-muted small mb-4 mb-lg-0">&copy; Your Website 2022. All Rights Reserved.</p>
                </div>
                <div class="col-lg-6 h-100 text-center text-lg-end my-auto">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item me-4">
                            <a href="#!"><i class="bi-facebook fs-3"></i></a>
                        </li>
                        <li class="list-inline-item me-4">
                            <a href="#!"><i class="bi-twitter fs-3"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#!"><i class="bi-instagram fs-3"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="api/api.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<!--     <script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
    <script src="assets/js/jquery_3.6.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
    <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
<script>
    $('#cmbDivision').on('change',function (){
        // alert('Test');
        let division_id = $('#cmbDivision').val();
        // alert(division_id);
       if (division_id){
           $.ajax
           ({
               url: 'districts/districts_dropdown.php',
               type:'post',
               data: {'division_id':division_id,},
               success:function (r)
               {
                   $('#cboDistricts').children().remove().end().html(r);
               }
           });
       }
    });

</script>
</body>

</html>