 <?php 
 require("../db_config.php");
 // Turn off all error reporting
 error_reporting(E_ERROR | E_PARSE);
 $division_id=$_POST["cmbDivision"];
 $districts_id=$_POST["cboDistricts"];
 $upazila_id=$_POST["cboUpazila"];
 echo $division_id."<br>".$districts_id."<br>".$upazila_id;
 
 $division_sql="SELECT a.id,a.name,a.code as estblished,b.id as dsitricts_id,b.name as districts,c.id as upazila_id,c.name as upazila,c.km 
 from divisions a,districts b,upazilas c
 where a.id=$division_id 
 and b.division_id=a.id 
 and c.district_id in (b.id)";

$result_division=$db->query($division_sql);
$all_data_arr=array();
$division_count=array();
$disticts_count=array();
 foreach ($result_division as $value) 
 {
      $all_data_arr[$value["id"]][$value["dsitricts_id"]][]=$value;
      $division_count[$value["id"]]++;
      $disticts_count[$value["dsitricts_id"]]++;
 }
//  $districts=implode(", ",array_unique(explode(",",chop($districts,','))));
 ?>
 <table class="table table-hover table-striped border table-bordered br">
  <thead>
    <tr valign='middle'>
        <th rowspan='2 ' style='align-content: center; '>Sl No</th>
        <th rowspan='2 '>Division Name</th>
        <th rowspan='2 '>Capital</th>
        <th rowspan='2 '>Established</th>
        <th colspan='2 ' style='border-top: 2px solid gray; border-left: 2px solid gray; border-right: 2px solid gray;border-bottom: 2px solid gray; '>Sub-Divisions</th>
        <th rowspan='2 '>Area (km<sup>2</sup>)</th>
    </tr>
    <tr>
      <th style='border-right: 2px solid gray;border-bottom: 2px solid gray; border-left: 2px solid gray;border-bottom: 2px solid gray; '>Districts</th>
      <th style='border-right: 2px solid gray;border-bottom: 2px solid gray; '>Upazilas</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $i=1;
    $division_chk=array();
    $district_chk=array();
    foreach ($all_data_arr as $division_key => $division_value) 
    {
        foreach ($division_value as $district_key => $district_value) 
        {
          foreach ($district_value as $row) 
          {
              ?>
              <tr>
                  <?php 
                  if(!in_array($division_key,$division_chk))
                  {
                      $division_chk[]=$division_key;
                      ?>
                      <td rowspan="<?php echo $division_count[$division_key];?>"><?php echo $i;?></td>

                      <td rowspan="<?php echo $division_count[$division_key];?>"><?php echo $row["name"];?></td>

                      <td rowspan="<?php echo $division_count[$division_key];?>"><?php echo $row["name"];?></td>

                      <td rowspan="<?php echo $division_count[$division_key];?>"><?php echo $row["estblished"];?></td>
                    <?php
                    }
                    if(!in_array($district_key,$district_chk))
                    {
                      $district_chk[]=$district_key;
                      ?>
                      <td rowspan="<?php echo $disticts_count[$district_key];?>"><?php echo $row["districts"];?></td>
                        <?php
                    }
                  ?>
                 <td><?php echo $row["upazila"];?></td>
                 <td><?php echo $row["km"];?></td>
              </tr>
              <?php
              $i++;
          }  
        }
    }
    ?>
  </tbody>
</table>